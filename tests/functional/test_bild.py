"""
This file (test_directory.py) contains the functional tests for the bild module.
"""
import os

import pytest

from bild.directory import Directory


FIXTURE_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'phone_pictures', '')


@pytest.mark.datafiles(FIXTURE_DIR, keep_top_dir=True)
def test_various_files(datafiles, tmpdir):
    """
    GIVEN a new picture file
    WHEN the creation date is extracted from the metadata
    THEN check the creation date extracted is correct
    """
    for directory in datafiles.listdir():
        print(directory)
        backup_pictures = tmpdir.mkdir('Pictures')
        backup_videos = tmpdir.mkdir('Videos')
        new_directory = Directory(str(directory),        # Source Directory
                                  str(backup_pictures),  # Destination Directory (Pictures)
                                  str(backup_videos))    # Destination Directory (Videos)
        new_directory.print_summary()
        assert len(new_directory.files) == 7
        assert new_directory.number_of_picture_files == 5
        assert new_directory.number_of_video_files == 2
        new_directory.copy_files()
        assert new_directory.files_copied == 7
        assert new_directory.files_not_copied == 0
